var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM food';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(food_id, callback) {
    var query = 'SELECT f.*, r.name FROM food f ' +
        'LEFT JOIN restaurant_has rh on rh.food_id = f.food_id ' +
        'LEFT JOIN restaurant r on r.restaurant_id = rh.restaurant_id ' +
        'WHERE f.food_id = ?';
    var queryData = [food_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO food (name, sweet, salty, greasy, rich, warmth, spicy, rating) VALUES (?)';

    var queryData = [params.name, params.sweet, params.salty, params.greasy, params.rich, params.warmth, params.spicy, params.rating];

    connection.query(query, [queryData], function(err, result) {

        var food_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO restaurant_has (food_id, restaurant_id) VALUES ?';

        var restaurant_hasData = [];
        for(var i=0; i < params.restaurant_id.length; i++) {
            restaurant_hasData.push([food_id, params.restaurant_id[i]]);
        }

        connection.query(query, [restaurant_hasData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(food_id, callback) {
    var query = 'DELETE FROM food WHERE food_id = ?';
    var queryData = [food_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var restaurant_hasInsert = function(food_id, restaurantIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO restaurant_has (food_id, restaurant_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var restaurant_hasData = [];
    for(var i=0; i < restaurantIdArray.length; i++) {
        restaurant_hasData.push([food_id, restaurantIdArray[i]]);
    }
    connection.query(query, [restaurant_hasData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.restaurant_hasInsert = restaurant_hasInsert;

//declare the function so it can be used locally
var restaurant_hasDeleteAll = function(food_id, callback){
    var query = 'DELETE FROM restaurant_has WHERE food_id = ?';
    var queryData = [food_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.restaurant_hasDeleteAll = restaurant_hasDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE food SET name = ? WHERE food_id = ?';
    var queryData = [params.name, params.food_id];

    connection.query(query, queryData, function(err, result) {
        restaurant_hasDeleteAll(params.food_id, function(err, result){

            if(params.restaurant_id != null) {
                restaurant_hasInsert(params.food_id, params.restaurant_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

exports.edit = function(food_id, callback) {
    var query = 'CALL foodRestaurant_getinfo(?)';
    var queryData = [food_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};