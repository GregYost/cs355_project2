var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM food_truck;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(foodTruck_id, callback) {
    var query = 'SELECT * FROM food_truck;';
    var queryData = [foodTruck_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO food_truck (name, price_range, payment_method, city) VALUES (?)';

    var queryData = [params.name, params.price_range, params.payment_method, params.city];

    connection.query(query, [queryData], function(err, result) {

        callback(err, result);

    });
};

exports.delete = function(foodTruck_id, callback) {
    var query = 'DELETE FROM food_truck WHERE foodTruck_id = ?';
    var queryData = [foodTruck_id];

    connection.query(query, queryData, function(err, result) {

        callback(err, result);



    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE food_truck SET name = ? WHERE foodTruck_id = ?';
    var queryData = [params.name, params.foodTruck_id];
    connection.query(query, queryData, function (err, result) {
            var query = 'UPDATE food_truck SET price_range =? WHERE foodTruck_id =?';
            var queryData = [params.price_range, params.foodTruck_id];
            connection.query(query, queryData, function (err, result) {
                var query = 'UPDATE food_truck SET payment_method =? WHERE foodTruck_id =?';
                var queryData = [params.payment_method, params.foodTruck_id];
                connection.query(query, queryData, function (err, result) {
                        var query = 'UPDATE food_truck SET zip =? WHERE foodTruck_id =?';
                        var queryData = [params.city, params.foodTruck_id];
                        connection.query(query, queryData, function (err, result) {
                            callback(err, result);
                        });
                    });
                });
            });
};

exports.edit = function(foodTruck_id, callback) {
    var query = 'CALL foodTruck_getinfo(?)';
    var queryData = [foodTruck_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};