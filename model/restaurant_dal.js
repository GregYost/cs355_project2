var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM restaurant;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(restaurant_id, callback) {
    var query = 'SELECT * FROM restaurant;';
    var queryData = [restaurant_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO restaurant (name, type, price_range, payment_method, street_No, zip) VALUES (?)';

    var queryData = [params.name, params.type, params.price_range, params.payment_method, params.street_No, params.zip];

    connection.query(query, [queryData], function(err, result) {

        callback(err, result);

    });
};

exports.delete = function(restaurant_id, callback) {
    var query = 'DELETE FROM restaurant WHERE restaurant_id = ?';
    var queryData = [restaurant_id];

    connection.query(query, queryData, function(err, result) {

        callback(err, result);



    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE restaurant SET name = ? WHERE restaurant_id = ?';
    var queryData = [params.name, params.restaurant_id];
    connection.query(query, queryData, function (err, result) {
        var query = 'UPDATE restaurant SET type =? WHERE restaurant_id =?';
        var queryData = [params.type, params.restaurant_id];
        connection.query(query, queryData, function (err, result) {
            var query = 'UPDATE restaurant SET price_range =? WHERE restaurant_id =?';
            var queryData = [params.price_range, params.restaurant_id];
            connection.query(query, queryData, function (err, result) {
                var query = 'UPDATE restaurant SET payment_method =? WHERE restaurant_id =?';
                var queryData = [params.payment_method, params.restaurant_id];
                connection.query(query, queryData, function (err, result) {
                    var query = 'UPDATE restaurant SET street_No =? WHERE restaurant_id =?';
                    var queryData = [params.street_No, params.restaurant_id];
                    connection.query(query, queryData, function (err, result) {
                        var query = 'UPDATE restaurant SET zip =? WHERE restaurant_id =?';
                        var queryData = [params.zip, params.restaurant_id];
                        connection.query(query, queryData, function (err, result) {
                            callback(err, result);
                        });
                    });
                });
            });
        });
    });
};

exports.edit = function(restaurant_id, callback) {
    var query = 'CALL restaurant_getinfo(?)';
    var queryData = [restaurant_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};