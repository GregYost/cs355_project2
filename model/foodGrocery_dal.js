var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM food;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(food_id, callback) {
    var query = 'SELECT f.*, g.name FROM food f ' +
        'LEFT JOIN grocery_has gh on gh.food_id = f.food_id ' +
        'LEFT JOIN grocery_store g on g.grocery_id = gh.grocery_id ' +
        'WHERE f.food_id = ?';
    var queryData = [food_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO food (name, sweet, salty, greasy, rich, warmth, spicy, rating) VALUES (?)';

    var queryData = [params.name, params.sweet, params.salty, params.greasy, params.rich, params.warmth, params.spicy, params.rating];

    connection.query(query, [queryData], function(err, result) {

        var food_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO grocery_has (food_id, grocery_id) VALUES ?';

        var grocery_hasData = [];
        for(var i=0; i < params.grocery_id.length; i++) {
            grocery_hasData.push([food_id, params.grocery_id[i]]);
        }

        connection.query(query, [grocery_hasData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(food_id, callback) {
    var query = 'DELETE FROM food WHERE food_id = ?';
    var queryData = [food_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var grocery_hasInsert = function(food_id, groceryIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO grocery_has (food_id, grocery_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var grocery_hasData = [];
    for(var i=0; i < groceryIdArray.length; i++) {
        grocery_hasData.push([food_id, groceryIdArray[i]]);
    }
    connection.query(query, [grocery_hasData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.grocery_hasInsert = grocery_hasInsert;

//declare the function so it can be used locally
var grocery_hasDeleteAll = function(food_id, callback){
    var query = 'DELETE FROM grocery_has WHERE food_id = ?';
    var queryData = [food_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.grocery_hasDeleteAll = grocery_hasDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE food SET name = ? WHERE food_id = ?';
    var queryData = [params.name, params.food_id];

    connection.query(query, queryData, function(err, result) {
        grocery_hasDeleteAll(params.food_id, function(err, result){

            if(params.grocery_id != null) {
                grocery_hasInsert(params.food_id, params.grocery_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

exports.edit = function(food_id, callback) {
    var query = 'CALL foodGrocery_getinfo(?)';
    var queryData = [food_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};