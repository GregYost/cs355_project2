var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM grocery_store;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(grocery_id, callback) {
    var query = 'SELECT * FROM grocery_store;';
    var queryData = [grocery_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO grocery_store (name, price_range, payment_method, street_No, zip) VALUES (?)';

    var queryData = [params.name, params.price_range, params.payment_method, params.street_No, params.zip];

    connection.query(query, [queryData], function(err, result) {

        callback(err, result);

    });
};

exports.delete = function(grocery_id, callback) {
    var query = 'DELETE FROM grocery_store WHERE grocery_id = ?';
    var queryData = [grocery_id];

    connection.query(query, queryData, function(err, result) {

        callback(err, result);



    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE grocery_store SET name = ? WHERE grocery_id = ?';
    var queryData = [params.name, params.grocery_id];
    connection.query(query, queryData, function (err, result) {
            var query = 'UPDATE grocery_store SET price_range =? WHERE grocery_id =?';
            var queryData = [params.price_range, params.grocery_id];
            connection.query(query, queryData, function (err, result) {
                var query = 'UPDATE grocery_store SET payment_method =? WHERE grocery_id =?';
                var queryData = [params.payment_method, params.grocery_id];
                connection.query(query, queryData, function (err, result) {
                    var query = 'UPDATE grocery_store SET street_No =? WHERE grocery_id =?';
                    var queryData = [params.street_No, params.grocery_id];
                    connection.query(query, queryData, function (err, result) {
                        var query = 'UPDATE grocery_store SET zip =? WHERE grocery_id =?';
                        var queryData = [params.zip, params.grocery_id];
                        connection.query(query, queryData, function (err, result) {
                            callback(err, result);
                        });
                    });
                });
            });
        });
};

exports.edit = function(grocery_id, callback) {
    var query = 'CALL grocery_getinfo(?)';
    var queryData = [grocery_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};