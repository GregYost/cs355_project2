var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM food';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(food_id, callback) {
    var query = 'SELECT f.*, ft.name FROM food f ' +
        'LEFT JOIN foodTruck_has fh on fh.food_id = f.food_id ' +
        'LEFT JOIN food_truck ft on ft.foodTruck_id = fh.foodTruck_id ' +
        'WHERE f.food_id = ?';
    var queryData = [food_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO food (name, sweet, salty, greasy, rich, warmth, spicy, rating) VALUES (?)';

    var queryData = [params.name, params.sweet, params.salty, params.greasy, params.rich, params.warmth, params.spicy, params.rating];

    connection.query(query, [queryData], function(err, result) {

        var food_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO foodTruck_has (food_id, foodTruck_id) VALUES ?';

        var foodTruck_hasData = [];
        for(var i=0; i < params.foodTruck_id.length; i++) {
            foodTruck_hasData.push([food_id, params.foodTruck_id[i]]);
        }

        connection.query(query, [foodTruck_hasData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(food_id, callback) {
    var query = 'DELETE FROM food WHERE food_id = ?';
    var queryData = [food_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var foodTruck_hasInsert = function(food_id, foodTruckIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO foodTruck_has (food_id, foodTruck_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var foodTruck_hasData = [];
    for(var i=0; i < foodTruckIdArray.length; i++) {
        foodTruck_hasData.push([food_id, foodTruckIdArray[i]]);
    }
    connection.query(query, [foodTruck_hasData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.foodTruck_hasInsert = foodTruck_hasInsert;

//declare the function so it can be used locally
var foodTruck_hasDeleteAll = function(food_id, callback){
    var query = 'DELETE FROM foodTruck_has WHERE food_id = ?';
    var queryData = [food_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.foodTruck_hasDeleteAll = foodTruck_hasDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE food SET name = ? WHERE food_id = ?';
    var queryData = [params.name, params.food_id];

    connection.query(query, queryData, function(err, result) {
        foodTruck_hasDeleteAll(params.food_id, function(err, result){

            if(params.foodTruck_id != null) {
                foodTruck_hasInsert(params.food_id, params.foodTruck_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

exports.edit = function(food_id, callback) {
    var query = 'CALL foodfoodTruck_getinfo(?)';
    var queryData = [food_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};