var express = require('express');
var router = express.Router();
var foodRatingRange_dal = require('../model/foodRatingRange_dal');

router.get('/all', function(req, res) {
    foodRatingRange_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('foodRatingRange/foodRatingRangeViewAll', { 'result':result });
        }
    });

});

module.exports = router;
