/**
 * Created by Greg on 4/23/2017.
 */
var express = require('express');
var router = express.Router();
var restaurant_dal = require('../model/restaurant_dal');

router.get('/all', function(req, res) {
    restaurant_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('restaurant/restaurantViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.restaurant_id == null) {
        res.send('restaurant_id is null');
    }
    else {
        restaurant_dal.getById(req.query.restaurant_dal_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('restaurant/restaurantViewById', {'result': result});
            }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    restaurant_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('restaurant/restaurantAdd', {'restaurant': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.name == null) {
        res.send('name must be provided.');
    }
    else if(req.query.type == null) {
        res.send('At least one type must be selected');
    }
    else if(req.query.price_range == null) {
        res.send('At least one price_range must be selected');
    }
    else if(req.query.payment_method == null) {
        res.send('At least one payment_method must be selected');
    }
    else if(req.query.street_No == null) {
        res.send('At least one street_No must be selected');
    }
    else if(req.query.zip == null) {
        res.send('At least one zip must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        restaurant_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                //res.redirect(302, '/restaurant/all');
                res.send("successfully added restaurant!");
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.restaurant_id == "") {
        res.send('A restaurant id is required');
    }
    else if(req.query.name == "") {
        res.send('A name is required');
    }
    else if(req.query.type == "") {
        res.send('A type is required');
    }
    else if(req.query.price_range == "") {
        res.send('Adam says a price_range is required');
    }
    else if(req.query.payment_method == "") {
        res.send('Adam says a payment_method is required');
    }
    else if(req.query.street_No == "") {
        res.send('Adam says a street_No is required');
    }
    else if(req.query.zip == "") {
        res.send('Adam says a zip is required');
    }
    else {
        restaurant_dal.edit(req.query.restaurant_id, function(err, result){
            res.render('restaurant/restaurantUpdate', {restaurant: result[0][0]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.restaurant_id == null) {
        res.send('A company id is required');
    }
    else {
        restaurant_dal.getById(req.query.restaurant_id, function(err, restaurant){
            restaurant_dal.getAll(function(err, restaurant) {
                res.render('restaurant/restaurantUpdate', {restaurant: restaurant[0]});
            });
        });
    }

});

router.get('/update', function(req, res) {
    restaurant_dal.update(req.query, function(err, result){
        res.redirect(302, '/restaurant/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.restaurant_id == null) {
        res.send('restaurant_id is null');
    }
    else {
        restaurant_dal.delete(req.query.restaurant_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/restaurant/all');
            }
        });
    }
});

module.exports = router;
