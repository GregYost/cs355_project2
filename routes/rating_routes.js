var express = require('express');
var router = express.Router();
var rating_dal = require('../model/rating_dal');

router.get('/all', function(req, res) {
    rating_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('rating/ratingViewAll', { 'result':result });
        }
    });

});

module.exports = router;
