var express = require('express');
var router = express.Router();
var foodGrocery_dal = require('../model/foodGrocery_dal');
var grocery_dal = require('../model/grocery_dal');

router.get('/all', function(req, res) {
    foodGrocery_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('foodGrocery/foodGroceryViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.food_id == null) {
        res.send('food_id is null');
    }
    else {
        foodGrocery_dal.getById(req.query.food_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('foodGrocery/foodGroceryViewById', {'result': result});
           }
        });
    }
});

router.get('/add', function(req, res){
    grocery_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('foodGrocery/foodGroceryAdd', {'grocery': result});
        }
    });
});

router.get('/insert', function(req, res){
    // simple validation
    if(req.query.name == null) {
        res.send('Food Name must be provided.');
    }
    else if(req.query.grocery_id == null) {
        res.send('At least one grocery store must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        foodGrocery_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                //res.redirect(302, '/foodGrocery/all');
                res.send("successfully added food!");
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.food_id == null) {
        res.send('A food id is required');
    }
    else {
        foodGrocery_dal.edit(req.query.food_id, function(err, result){
            res.render('foodGrocery/foodGroceryUpdate', {food: result[0][0], grocery: result[1]});
        });
    }

});

router.get('/update', function(req, res) {
    foodGrocery_dal.update(req.query, function(err, result){
       res.redirect(302, '/foodGrocery/all');
    });
});

router.get('/delete', function(req, res){
    if(req.query.food_id == null) {
        res.send('food_id is null');
    }
    else {
        foodGrocery_dal.delete(req.query.food_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/foodGrocery/all');
             }
         });
    }
});

module.exports = router;
