var express = require('express');
var router = express.Router();
var foodRestaurant_dal = require('../model/foodRestaurant_dal');
var restaurant_dal = require('../model/restaurant_dal');

router.get('/all', function(req, res) {
    foodRestaurant_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('foodRestaurant/foodRestaurantViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.food_id == null) {
        res.send('food_id is null');
    }
    else {
        foodRestaurant_dal.getById(req.query.food_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('foodRestaurant/foodRestaurantViewById', {'result': result});
           }
        });
    }
});

router.get('/add', function(req, res){
    restaurant_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('foodRestaurant/foodRestaurantAdd', {'restaurant': result});
        }
    });
});

router.get('/insert', function(req, res){
    // simple validation
    if(req.query.name == null) {
        res.send('Food Name must be provided.');
    }
    else if(req.query.restaurant_id == null) {
        res.send('At least one restaurant must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        foodRestaurant_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                //res.redirect(302, '/foodRestaurant/all');
                res.send("successfully added food!");
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.food_id == null) {
        res.send('A food id is required');
    }
    else {
        foodRestaurant_dal.edit(req.query.food_id, function(err, result){
            res.render('foodRestaurant/foodRestaurantUpdate', {food: result[0][0], restaurant: result[1]});
        });
    }

});

router.get('/update', function(req, res) {
    foodRestaurant_dal.update(req.query, function(err, result){
       res.redirect(302, '/foodRestaurant/all');
    });
});

router.get('/delete', function(req, res){
    if(req.query.food_id == null) {
        res.send('food_id is null');
    }
    else {
        foodRestaurant_dal.delete(req.query.food_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/foodRestaurant/all');
             }
         });
    }
});

module.exports = router;
