/**
 * Created by Greg on 4/23/2017.
 */
var express = require('express');
var router = express.Router();
var foodTruck_dal = require('../model/foodTruck_dal');

router.get('/all', function(req, res) {
    foodTruck_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('foodTruck/foodTruckViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.foodTruck_id == null) {
        res.send('foodTruck_id is null');
    }
    else {
        foodTruck_dal.getById(req.query.foodTruck_dal_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('foodTruck/foodTruckViewById', {'result': result});
            }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    foodTruck_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('foodTruck/foodTruckAdd', {'foodTruck': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.name == null) {
        res.send('name must be provided.');
    }
    else if(req.query.price_range == null) {
        res.send('At least one price_range must be selected');
    }
    else if(req.query.payment_method == null) {
        res.send('At least one payment_method must be selected');
    }
    else if(req.query.city == null) {
        res.send('At least one city must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        foodTruck_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                //res.redirect(302, '/foodTruck/all');
                res.send("successfully added food truck!");
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.foodTruck_id == "") {
        res.send('A foodTruck id is required');
    }
    else if(req.query.name == "") {
        res.send('A name is required');
    }
    else if(req.query.price_range == "") {
        res.send('Adam says a price_range is required');
    }
    else if(req.query.payment_method == "") {
        res.send('Adam says a payment_method is required');
    }
    else if(req.query.city == "") {
        res.send('Adam says a city is required');
    }
    else {
        foodTruck_dal.edit(req.query.foodTruck_id, function(err, result){
            res.render('foodTruck/foodTruckUpdate', {foodTruck: result[0][0]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.foodTruck_id == null) {
        res.send('A company id is required');
    }
    else {
        foodTruck_dal.getById(req.query.foodTruck_id, function(err, foodTruck){
            foodTruck_dal.getAll(function(err, foodTruck) {
                res.render('foodTruck/foodTruckUpdate', {foodTruck: foodTruck[0]});
            });
        });
    }

});

router.get('/update', function(req, res) {
    foodTruck_dal.update(req.query, function(err, result){
        res.redirect(302, '/foodTruck/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.foodTruck_id == null) {
        res.send('foodTruck_id is null');
    }
    else {
        foodTruck_dal.delete(req.query.foodTruck_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/foodTruck/all');
            }
        });
    }
});

module.exports = router;
