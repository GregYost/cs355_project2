/**
 * Created by Greg on 4/23/2017.
 */
var express = require('express');
var router = express.Router();
var grocery_dal = require('../model/grocery_dal');

router.get('/all', function(req, res) {
    grocery_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('grocery/groceryViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.grocery_id == null) {
        res.send('grocery_id is null');
    }
    else {
        grocery_dal.getById(req.query.grocery_dal_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('grocery/groceryViewById', {'result': result});
            }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    grocery_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('grocery/groceryAdd', {'grocery': result});
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.name == null) {
        res.send('name must be provided.');
    }
    else if(req.query.price_range == null) {
        res.send('At least one price_range must be selected');
    }
    else if(req.query.payment_method == null) {
        res.send('At least one payment_method must be selected');
    }
    else if(req.query.street_No == null) {
        res.send('At least one street_No must be selected');
    }
    else if(req.query.zip == null) {
        res.send('At least one zip must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        grocery_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                //res.redirect(302, '/grocery/all');
                res.send("successfully added grocery!");
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.grocery_id == "") {
        res.send('A grocery id is required');
    }
    else if(req.query.name == "") {
        res.send('A name is required');
    }
    else if(req.query.price_range == "") {
        res.send('Adam says a price_range is required');
    }
    else if(req.query.payment_method == "") {
        res.send('Adam says a payment_method is required');
    }
    else if(req.query.street_No == "") {
        res.send('Adam says a street_No is required');
    }
    else if(req.query.zip == "") {
        res.send('Adam says a zip is required');
    }
    else {
        grocery_dal.edit(req.query.grocery_id, function(err, result){
            res.render('grocery/groceryUpdate', {grocery: result[0][0]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.grocery_id == null) {
        res.send('A company id is required');
    }
    else {
        grocery_dal.getById(req.query.grocery_id, function(err, grocery){
            grocery_dal.getAll(function(err, grocery) {
                res.render('grocery/groceryUpdate', {grocery: grocery[0]});
            });
        });
    }

});

router.get('/update', function(req, res) {
    grocery_dal.update(req.query, function(err, result){
        res.redirect(302, '/grocery/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.grocery_id == null) {
        res.send('grocery_id is null');
    }
    else {
        grocery_dal.delete(req.query.grocery_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/grocery/all');
            }
        });
    }
});

module.exports = router;
