var express = require('express');
var router = express.Router();
var foodFoodTruck_dal = require('../model/foodFoodTruck_dal');
var foodTruck_dal = require('../model/foodTruck_dal');

router.get('/all', function(req, res) {
    foodFoodTruck_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('foodFoodTruck/foodFoodTruckViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.food_id == null) {
        res.send('food_id is null');
    }
    else {
        foodFoodTruck_dal.getById(req.query.food_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('foodFoodTruck/foodFoodTruckViewById', {'result': result});
           }
        });
    }
});

router.get('/add', function(req, res){
    foodTruck_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('foodFoodTruck/foodFoodTruckAdd', {'foodTruck': result});
        }
    });
});

router.get('/insert', function(req, res){
    // simple validation
    if(req.query.name == null) {
        res.send('Food Name must be provided.');
    }
    else if(req.query.foodTruck_id == null) {
        res.send('At least one foodTruck must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        foodFoodTruck_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                //res.redirect(302, '/foodFoodTruck/all');
                res.send("successfully added food!");
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.food_id == null) {
        res.send('A food id is required');
    }
    else {
        foodFoodTruck_dal.edit(req.query.food_id, function(err, result){
            res.render('foodFoodTruck/foodFoodTruckUpdate', {food: result[0][0], foodTruck: result[1]});
        });
    }

});

router.get('/update', function(req, res) {
    foodFoodTruck_dal.update(req.query, function(err, result){
       res.redirect(302, '/foodFoodTruck/all');
    });
});

router.get('/delete', function(req, res){
    if(req.query.food_id == null) {
        res.send('food_id is null');
    }
    else {
        foodFoodTruck_dal.delete(req.query.food_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/foodFoodTruck/all');
             }
         });
    }
});

module.exports = router;
